/**
 * Created by jacek on 27.05.15.
 */

var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;

module.exports = Object.create(Model, {
   table: {value: 'p_recenzenta'},
    add: {
        value: function (reviewer, callback) {
            var that = this;
            this.selectMaxInColumn('id_przypisania', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    reviewer.id_przypisania = 0;
                else
                    reviewer.id_przypisania = max + 1;
                that.insert(reviewer, function (result) {
                    callback(reviewer.id_przypisania);
                });
            });
        }
    },

    deleteReviewer: {
        value: function (id,callback) {
        this.connection.query('DELETE FROM ?? WHERE id_przypisania = ?',
            [this.table ,id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
    updateReviewer: {
        value: function (id, reviewer, callback) {
        this.connection.query('UPDATE ?? SET ? WHERE id_przypisania = ?',[this.table , reviewer, id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    }
    
});
