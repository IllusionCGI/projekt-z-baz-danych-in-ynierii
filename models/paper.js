/**
 * Created by jacek on 2015-05-12.
 */
 
var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;


module.exports = Object.create(Model, {
    table: {value: 'referat'},
    add: {
        value: function (paper, callback) {
            var that = this;
            this.selectMaxInColumn('id_referat', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    paper.id_referat = 0;
                else
                    paper.id_referat = max + 1;
                that.insert(paper, function (result) {
                    callback(paper.id_referat);
                });
            });
        }
    },

    selectAllPapers:{
        value:function(callback) {
        this.connection.query('SELECT r.id_referat, r.nazwa, r.poprawiony, r.zatwierdzony ,u.imie,u.nazwisko FROM uzytkownik u,referat r, przypisanie p WHERE r.przypisanie_id_przypisanie =p.id_przypisanie AND p.uzytkownik_id_uzytkownika=id_uzytkownika', function(err,rows,fields){
        callback(rows);
            });
        }
    },
    selectAllReviewers:{
        value:function(callback){
        this.connection.query('SELECT * FROM uzytkownik u WHERE u.recenzent = 1', function(err,rows,fields){
        callback(rows);
             });
        }
    },
    countAllAssignedReviewers:{
        value:function(field,callback){
        this.connection.query('SELECT COUNT(*) FROM p_recenzenta WHERE referat_id_referat = ? ',[field], function (err, rows, fields) {
                if (err)
                    throw err;
                else
                    callback(rows[0]['COUNT(*)']);
            });
        }
    },
    selectAllAssignedReviewers:{
        value:function(field,callback){
        this.connection.query('SELECT * FROM p_recenzenta WHERE referat_id_referat = ?',[field], function(err,rows,fields) {
            if (err)
                    throw err;
                else
                    callback(rows);
        });
        }
        
        
    },
    
    SelectAllArticles: 
    {
        value:function(callback){
            this.connection.query('select s.nazwa as nazwa, u.imie as imie, u.nazwisko as nazwisko, r.nazwa as tytul, (o.ocena_zgodnosci+o.ocena_oryginalnosci+o.ocena_jakosci+o.ocena_poprawnosci)/4 as ocena_sr from referat r, uzytkownik u join uczestnik ucz on u.id_uzytkownika=ucz.id_uczestnik, recenzja o, sesja s', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    getAllAssignedToReviewer: {
        value: function(user_id, callback) {
            this.connection.query('SELECT r.id_referat, r.nazwa, p.id_przypisania FROM p_recenzenta p, referat r ' + 
                                'WHERE r.id_referat = p.referat_id_referat ' +
                                'AND p.uzytkownik_id_uzytkownika = ? AND ' +
                                'p.id_przypisania not in (SELECT p_recenzenta_id_przypisania FROM recenzja)',
                                [user_id], function(err, rows, fields) {
                                    if(err)
                                        throw err;
                                    callback(rows);
                                });
        }
    },
     getAllAssignedConferences: {
        value: function(user_id, callback) {
            this.connection.query('select konferencja_id_konferencj from przypisanie where uzytkownik_id_uzytkownika = ?',
                                [user_id], function(err, rows, fields) {
                                    if(err)
                                        throw err;
                                    callback(rows);
                                });
        }
    },
    selectAssignement:{
        value: function(id_user,id_conf, callback) {
            this.connection.query('SELECT id_przypisanie FROM przypisanie WHERE uzytkownik_id_uzytkownika= ? and konferencja_id_konferencja = ?', [id_user,id_conf], function(err, rows, fields) {
                                    if(err)
                                        throw err;
                                    callback(rows[0]);
                                });
        }
        
    },
    getCreatedPapers:{
        value: function(user_id, callback){
            console.log(user_id);
            this.connection.query('SELECT r.nazwa, r.id_referat FROM referat r, przypisanie p WHERE r.przypisanie_id_przypisanie=p.id_przypisanie and p.uzytkownik_id_uzytkownika = ?',
                                [user_id.id_uzytkownika], function(err, rows, fields) {
                                    if(err)
                                        throw err;
                                    callback(rows);
                                });
        }
    },
    
    deletePaper: {
        value: function (id,callback) {
        this.connection.query('DELETE FROM ?? WHERE id_referat = ?',[this.table ,id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
    updatePaper: {
        value: function (id, paper, callback) {
        this.connection.query('UPDATE ?? SET ? WHERE id_referat = ?',[this.table , paper, id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
});
