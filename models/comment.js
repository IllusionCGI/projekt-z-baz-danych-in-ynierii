/**
 * Created by jacek on 27.05.15.
 */

var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;

module.exports = Object.create(Model, {
    table: {value: 'komentarz'},
    add: {
        value: function(obj, callback) {
            var that = this
            this.selectMaxInColumn('id_komentarz', function (err, max) {
                var id;
                if (err === ErrorCode.NOT_FOUND)
                    obj.id_komentarz = 0;
                else
                    obj.id_komentarz = max + 1;
                that.connection.query('INSERT INTO ?? SET data = NOW(), ?',
                [that.table, obj], function (err, result) {
                    if (err)
                        throw err;
                    else
                        callback(result);
                });    
            });
        }
    },
    getForReview: {
        value: function(review_id, callback) {
            this.connection.query('SELECT * FROM ?? WHERE recenzja_id_recenzja = ?',
            [this.table, review_id], function(err, rows, fields) {
                if(err)
                    throw(err)
                callback(rows);    
            });
        }
    }
});
